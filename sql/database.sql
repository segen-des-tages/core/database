CREATE TABLE `languages` (
    `key` varchar(2) PRIMARY KEY NOT NULL,
    `name` varchar(200) NOT NULL,
    `active` tinyint(1) NOT NULL
);

CREATE TABLE `blessings` (
    `id` int PRIMARY KEY AUTO_INCREMENT NOT NULL,
    `blessing` text NOT NULL,
    `language` varchar(2) NOT NULL
);

CREATE TABLE `calendar` (
    `id` int PRIMARY KEY AUTO_INCREMENT NOT NULL,
    `date` date NOT NULL,
    `language` varchar(2) NOT NULL,
    `blessing` int NOT NULL
);

ALTER TABLE `blessings`
ADD FOREIGN KEY (`language`) REFERENCES `languages` (`key`);

ALTER TABLE `calendar`
ADD FOREIGN KEY (`blessing`) REFERENCES `blessings` (`id`);

ALTER TABLE `calendar`
ADD FOREIGN KEY (`language`) REFERENCES `languages` (`key`);
