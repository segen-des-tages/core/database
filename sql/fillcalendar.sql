DELIMITER //

CREATE PROCEDURE FillCalendar(year INT, lang VARCHAR(2))
BEGIN
    DECLARE currentDay DATE;
    DECLARE endDate DATE;
    DECLARE isLeapYear BOOLEAN;
    DECLARE blessingID INT;

    -- Überprüfe, ob das Jahr ein Schaltjahr ist
    SET isLeapYear = (year % 4 = 0 AND year % 100 != 0) OR (year % 400 = 0);
    -- Setze das Enddatum basierend darauf, ob es ein Schaltjahr ist
    IF isLeapYear THEN
        SET endDate = CONCAT(year, '-12-31');
    ELSE
        SET endDate = CONCAT(year, '-12-31');
    END IF;

    SET currentDay = CONCAT(year, '-01-01');

    CREATE TEMPORARY TABLE IF NOT EXISTS usedBlessings (
        id INT
    );

    WHILE currentDay <= endDate DO
        -- Wähle einen zufälligen Segensspruch, der noch nicht verwendet wurde
        SELECT id INTO blessingID FROM blessings
        WHERE language = lang AND id NOT IN (SELECT id FROM usedBlessings)
        ORDER BY RAND() LIMIT 1;
        
        -- Wenn alle Segenssprüche verwendet wurden, leere die Tabelle usedBlessings
        IF blessingID IS NULL THEN
            TRUNCATE TABLE usedBlessings;
            SELECT id INTO blessingID FROM blessings
            WHERE language = lang
            ORDER BY RAND() LIMIT 1;
        END IF;

        -- Füge den ausgewählten Segensspruch in den Kalender ein
        INSERT INTO calendar (date, language, blessing) VALUES (currentDay, lang, blessingID);
        -- Füge die ID des verwendeten Segensspruchs zur temporären Tabelle hinzu
        INSERT INTO usedBlessings (id) VALUES (blessingID);

        -- Gehe zum nächsten Tag
        SET currentDay = ADDDATE(currentDay, 1);
    END WHILE;

    DROP TEMPORARY TABLE IF EXISTS usedBlessings;
END;
//

DELIMITER ;

DELIMITER //

CREATE PROCEDURE FillCalendarForActiveLanguages(IN year INT)
BEGIN
  DECLARE done INT DEFAULT FALSE;
  DECLARE langKey VARCHAR(2);
  DECLARE cur CURSOR FOR SELECT `key` FROM languages WHERE active = 1;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

  OPEN cur;

  read_loop: LOOP
    FETCH cur INTO langKey;
    IF done THEN
      LEAVE read_loop;
    END IF;
    CALL FillCalendar(year, langKey);
  END LOOP;

  CLOSE cur;
END;

//
DELIMITER ;
