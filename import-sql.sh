#!/bin/bash
set -e

# Wait for MariaDB to be ready
until mariadb -u root -p"$MARIADB_ROOT_PASSWORD" -e ";" ; do
  sleep 1
done

# Create Database segen_des_tages
echo "Creating Database and User segen_des_tages"
mariadb -u root -p"$MARIADB_ROOT_PASSWORD" -e "CREATE DATABASE IF NOT EXISTS segen_des_tages;"
mariadb -u root -p"$MARIADB_ROOT_PASSWORD" -e "CREATE USER IF NOT EXISTS segen_des_tages@'%' IDENTIFIED BY 'segen_des_tages'; "
mariadb -u root -p"$MARIADB_ROOT_PASSWORD" -e "GRANT SELECT ON segen_des_tages.* TO segen_des_tages@'%'; "

# Import Database
echo "Importing Structire"
mariadb -u root -p"$MARIADB_ROOT_PASSWORD" segen_des_tages < /tmp/database.sql
mariadb -u root -p"$MARIADB_ROOT_PASSWORD" segen_des_tages < /tmp/fillcalendar.sql

echo "Importing Content"
mariadb -u root -p"$MARIADB_ROOT_PASSWORD" segen_des_tages < /tmp/content/languages.sql
mariadb -u root -p"$MARIADB_ROOT_PASSWORD" segen_des_tages < /tmp/content/blessings/de.sql
mariadb -u root -p"$MARIADB_ROOT_PASSWORD" segen_des_tages < /tmp/content/calendar/de/2024.sql
mariadb -u root -p"$MARIADB_ROOT_PASSWORD" segen_des_tages < /tmp/content/calendar/de/2025.sql