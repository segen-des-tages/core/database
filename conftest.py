import os

# Define a method to add command-line options to pytest
def pytest_addoption(parser):
    parser.addoption("--host", action="store", 
                      default=os.getenv("DB_HOST", "localhost"), help="Database host")
    parser.addoption("--user", action="store", 
                      default=os.getenv("DB_USER", "segen_des_tages"), help="Database user")
    parser.addoption("--password", action="store", 
                      default=os.getenv("DB_PASSWORD", "segen_des_tages"), help="Database password")
    parser.addoption("--name", action="store",
                      default=os.getenv("DB_NAME", "segen_des_tages"), help="Database name")