[![OpenSSF Best Practices](https://www.bestpractices.dev/projects/8747/badge)](https://www.bestpractices.dev/projects/8747)

# Database

The main MariaDB Database with all Blessings. 

We are using SEMVER, so there is always a container with the latest version and one with the tag `latest`

[[_TOC_]]

## Structure

The database is structured in the following way:

- `blessings` - All Blessings with their language
- `languages` - All Languages and their active status
- `calendar` - All Days with their Blessing and Language

```plantuml

@startuml

entity "blessings" {
  + id [PK]
  --
  blessing
  language [FK]
}

entity "languages" {
  + key [PK]
  --
  name
  active
}

entity "calendar" {
  + id [PK]
  --
  date
  language [FK]
  blessing [FK]
}

blessings ||--o{ languages
calendar ||--o{ blessings
calendar ||--o{ languages

@enduml
```

```dbml
table languages {
  key varchar(2) [PK]
  name varchar(200)
  active boolean
}
table blessings {
  id int [PK]
  blessing text
  language varchar(2)
}

table calendar {
  id int [PK]
  date date
  language varchar(2)
  blessing int
}

ref: blessings.language > languages.key

ref: calendar.blessing > blessings.id

ref: calendar.language > languages.key
```

## Fill the calendar

To fill the calendar with the blessings, you can use the `fill_calendar` procedure.
  
  ```sql
  CALL FillCalendarForActiveLanguages(2024);
  ```

## Database Requirements and Tests

The following tests are implemented to ensure the database meets the necessary operational and security requirements:

### Operational Integrity Tests

1. **Active Languages Existence**
   - **Requirement**: There must be at least one language marked as active in the `languages` table.
   - **Test**: `test_active_languages`
   - **Description**: Checks if there is at least one active language available.

2. **Sufficient Blessings for Active Languages**
   - **Requirement**: Each active language must have at least 300 blessings associated with it.
   - **Test**: `test_blessings_per_active_language`
   - **Description**: Ensures that every active language in the `languages` table has a minimum of 300 blessings in the `blessings` table.

3. **Complete Calendar Entries for Current and Next Year**
   - **Requirement**: The `calendar` table must have a full set of entries for each day of the current and the following year.
   - **Test**: `test_full_calendar_for_years`
   - **Description**: Verifies that there are calendar entries for every day for the current and next year, ensuring no gaps.

### Relational Integrity Tests

4. **Valid Language References in Blessings**
   - **Requirement**: All entries in the `blessings` table must reference a valid `key` in the `languages` table.
   - **Test**: `test_language_reference_integrity`
   - **Description**: Checks that each blessing's language reference corresponds to an existing language key.

5. **Valid References in Calendar**
   - **Requirement**: All entries in the `calendar` table must reference existing IDs in both the `blessings` and `languages` tables.
   - **Test**: `test_calendar_integrity`
   - **Description**: Ensures that every calendar entry's language and blessing IDs are valid and existing.

### Security Tests

7. **No Unauthorized Modification (UPDATE)**
   - **Requirement**: `UPDATE` operations should not be permitted for any user on this database.
   - **Test**: `test_update_permissions`
   - **Description**: Attempts to update a record to verify that such actions are blocked.

8. **No Unauthorized Data Addition (INSERT)**
   - **Requirement**: `INSERT` operations should not be allowed.
   - **Test**: `test_insert_permissions`
   - **Description**: Attempts to insert a new record to confirm that the operation is blocked.

9. **No Unauthorized Data Deletion (DELETE)**
   - **Requirement**: `DELETE` operations should be prohibited.
   - **Test**: `test_delete_permissions`
   - **Description**: Attempts to delete a record to ensure that the operation is blocked.

### Additional Validations

11. **No Duplicate Blessings Per Day Per Language**
    - **Requirement**: There should be no duplicate blessings for any language on the same day.
    - **Test**: `test_no_duplicate_blessings_per_day_per_language`
    - **Description**: Verifies that no language has the same blessing assigned more than once on any given day.

12. **At Least One Blessing Per Day Per Active Language**
    - **Requirement**: Each active language should have at least one blessing assigned each day for at least the next year.
    - **Test**: `test_at_least_one_blessing_per_day_per_language`
    - **Description**: Ensures that every active language has at least one blessing per day for the next year, preventing gaps in daily blessings.

13. **Consistent Language in Blessings and Calendar**
    - **Requirement**: The language of a blessing referenced in the `calendar` should match the `calendar` entry's language.
    - **Test**: `test_blessing_language_consistency`
    - **Description**: Confirms that the language specified for a blessing in the `calendar` matches the blessing's language in the `blessings` table.

These tests are crucial for maintaining the integrity and security of the database, ensuring that data is consistent, correctly typed, and securely manipulated only through authorized operations. They are implemented using Pytest and can be run to validate environment setups and during continuous integration processes.


## Installation

There are several ways to install the database. The recommended method is to use Docker, but you can also install it manually.

### Docker / Podman

1. Install Docker or Podman
2. Pull the image from the registry: `docker pull registry.gitlab.com/segen-des-tages/core/database/database:latest` or `podman pull registry.gitlab.com/segen-des-tages/core/database/database:latest`
3. Run the Image
   1. Docker: `docker run -p 3306:3306 registry.gitlab.com/segen-des-tages/core/database/database:latest`
   2. Podman: `podman run -p 3306:3306 registry.gitlab.com/segen-des-tages/core/database/database:latest`

Password for the root user is `GGUnCA9hyCTN4KYaJEKXnuQT` and the database is called `segen_des_tages`.  
The Read-Only User is `segen_des_tages` with the password `segen_des_tages`.

### Manual Installation

1. Install mariadb
2. Clone the repository: `git clone https://gitlab.com/segen-des-tages/core/database.git`
3. Import the database: `mysql -u root -p < sql/database.sql`
4. Import the data: `mysql -u root -p < sql/content_*.sql`
5. Create a user
6. Grant the user access to the database
7. Done

## Local Testing

To run the database tests locally, ensure you have Python and `pytest` installed. Also, make sure your MySQL database is accessible via your local credentials. Here’s how you can run the tests:

1. Install pytest if you haven't already: `pip install pytest mysql-connector-python`
2. Run the tests using: `pytest test_database.py --host localhost --user segen_des_tages --password segen_des_tages --name segen_des_tages`


This will execute the tests defined in `test_database.py`, checking for active languages, blessings integrity, and calendar completeness.

## CI/CD Pipeline Configuration

Our CI/CD pipeline is configured to ensure thorough testing, secure building, and reliable deployment of our database and its associated containerized application. Below is a detailed explanation of each stage in the pipeline.

### Stages and Jobs

#### 1. Lint Stage
- **Lint MySQL Files and SAST**:
  - **Job**: `lint:sast`
  - **Purpose**: Lints MySQL files to ensure they follow best practices and checks the SQL code for potential security issues using SQLFluff, a linter for SQL languages.
  - **Environment**: Uses `python:3.9-slim` to minimize the footprint.
  - **Execution Condition**: Runs on all branches automatically whenever changes are pushed.

#### 2. Build Stage
- **Build Container and Push to Registry**:
  - **Job**: `build`
  - **Purpose**: Builds the Docker container from the Dockerfile (`Containerfile`), tags it with the current version, and pushes it to the GitLab container registry.
  - **Environment**: Uses `docker:19.03.12` with Docker-in-Docker (`dind`) service to allow Docker commands within the job.
  - **Execution Condition**: Only executes on pushes to the `develop` branch.

#### 3. Test Stage
- **Container Vulnerability Scan**:
  - **Job**: `test:container-scan`
  - **Purpose**: Scans the built Docker container for vulnerabilities using Trivy, focusing on high and critical severity issues.
  - **Environment**: Uses the `aquasec/trivy:latest` image.
  - **Execution Condition**: Runs for the `develop` branch.

- **Database Acceptance Test**:
  - **Job**: `test:acceptance-test`
  - **Purpose**: Ensures that the MySQL server in the container is functioning correctly by performing a basic SELECT operation.
  - **Environment**: Uses `mysql:latest` to perform the test.
  - **Execution Condition**: Only on the `develop` branch.

- **Database Integrity Test**:
  - **Job**: `test:integrity`
  - **Purpose**: Performs integrity checks on the database using pytest to validate operational and relational requirements.
  - **Environment**: Uses `python:3.9` for running pytest.
  - **Execution Condition**: Targets only the `develop` branch.

- **Dynamic Application Security Testing (DAST)**:
  - **Job**: `test:dast`
  - **Purpose**: Tests the database for SQL injection vulnerabilities using sqlmap, ensuring security against common SQL vulnerabilities.
  - **Environment**: Uses `alpine:latest` to keep the job lightweight.
  - **Execution Condition**: Executes for the `develop` branch.

#### 4. Deploy Stage
- **Create 'latest' Tag**:
  - **Job**: `create:latest`
  - **Purpose**: Tags the most recent container image as 'latest' and pushes it to the registry. This ensures that the 'latest' tag is always up-to-date with the most stable version.
  - **Environment**: Utilizes `docker:19.03.12` with Docker-in-Docker service.
  - **Execution Condition**: Executes when changes are pushed to the `main` branch.

- **Server Deployment**:
  - **Job**: `deploy:server`
  - **Purpose**: Handles the deployment of the 'latest' container image to the server using SSH commands.
  - **Environment**: Uses `alpine:latest` to execute SSH and deployment scripts.
  - **Execution Condition**: Runs post the `create:latest` job on pushes to `main`.

- **Release Creation**:
  - **Job**: `release`
  - **Purpose**: Creates a new release tag in the GitLab repository, marking a new stable release of the project.
  - **Environment**: Uses the `registry.gitlab.com/gitlab-org/release-cli:latest` for leveraging GitLab's release CLI.
  - **Execution Condition**: Follows the `deploy:server` job for finalization of the release process.


### Key Variables

- `CONTAINER_IMAGE`: Defines the path for the Docker image in the GitLab container registry.
- `MYSQL_*`: Variables for configuring the MySQL database connection used during


## Contributing

We are happy if you want to collaborate with us! Find all Details in the [Contributing Guide](CONTRIBUTING.md).

## License

Segen des Tages API is licensed under the [MIT License](LICENSE.md). Feel free to use, modify, and distribute it according to your needs.

## Authors

- **[Dominik Sigmund](https://gitlab.com/sigmund.dominik)** - Lead Designer and Developer

## Acknowledgments

We appreciate the collaborative efforts that have contributed to the success of Segen des Tages Database. We'd like to thank the following individuals and organizations for their support and contributions:

- (none yet, but you could be the first!)

## Support

If you can't find a solution to your issue in the documentation, feel free to reach out to us for assistance. We offer support through the following channels:

- **Discord:** Join our [Discord server](https://discord.gg/CrNxtpD6) to connect with the community and get help from the Segen-des-Tages team.

- **Email:** For non-urgent matters or if you prefer email communication, you can reach us at [database@segen-des-tages.de](mailto:database@segen-des-tages.de). Please provide detailed information about your issue so that we can assist you more effectively.

## Important Note

When seeking support, make sure to include the following information in your message:

1. A detailed description of the issue you're facing.
2. Steps to reproduce the problem (if applicable).
3. Relevant configuration details.
4. Any error messages or logs related to the issue.

This information will help us understand your situation better and provide a more accurate and timely response.

Thank you for choosing Segen des Tages!! We're committed to ensuring you have a positive experience, and we appreciate your cooperation in following these support guidelines.