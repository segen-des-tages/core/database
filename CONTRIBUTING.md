# Contributing

We appreciate and welcome contributions from the community to enhance the features and overall quality of Segen des Tages. Whether you're a developer, tester, or enthusiastic user, there are several ways you can contribute:

## Creating Issues

If you encounter a bug, have a feature request, or want to suggest improvements, please [create an issue](https://gitlab.com/segen-des-tages/core/database/-/issues/new) on our Gitlab repository. When creating an issue, provide detailed information about the problem or enhancement you're addressing. This includes steps to reproduce the issue and any relevant context that can help our team understand and address it effectively.

## Add a Language

Great, you want to add a Language!   
Follow our step-by-step Guide :-)

1. Create an Issue
2. Fork the repository.
3. Create a new branch for your changes.
4. Download and start the latest Container or the database as seen in the main branch
5. Insert your Language under `languages``
6. Insert at least 366 Blessings for you language
7. Create the calender using the procedure for at least this and the next year.
8. Run the local tests againt the database
9. Export your Content-Files and place them under __sql/content__
10. Update the file __sql/content/languages.sql__
11. Update the version.yml file, increment one minor
12. Update the CHANGELOG.md
13. Update the README if necessary
14. Submit a merge request to the `develop` branch of the repository.


## Pull Requests

If you'd like to contribute code, documentation, or fixes, we encourage you to submit a pull request. Before creating a pull request, please:

1. Fork the repository.
2. Create a new branch for your changes.
3. Make your modifications, ensuring adherence to our coding standards.
4. Write tests for new features or modifications.
5. Ensure all tests pass.
6. Update the version.yml file
7. Update the CHANGELOG.md
8. Update the README if necessary
9. Submit a merge request to the `develop` branch of the repository.

We'll review your pull request, provide feedback, and work with you to ensure that your contribution aligns with the project's goals and standards.

From there, we will merge it into `main`.
