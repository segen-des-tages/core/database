import pytest
import mysql.connector
from datetime import datetime


# Fixture to handle database connections based on the command-line args
@pytest.fixture
def db(pytestconfig):
    db_host = pytestconfig.getoption("host")
    db_user = pytestconfig.getoption("user")
    db_password = pytestconfig.getoption("password")
    db_name = pytestconfig.getoption("name")

    conn = mysql.connector.connect(
        host=db_host,
        user=db_user,
        password=db_password,
        database=db_name
    )
    yield conn
    conn.close()

def test_active_languages(db):
    cursor = db.cursor()
    cursor.execute("SELECT COUNT(*) FROM languages WHERE active = 1")
    (number_of_active_languages,) = cursor.fetchone()
    assert number_of_active_languages > 0, "There should be at least one active language."
    cursor.close()

def test_blessings_per_active_language(db):
    cursor = db.cursor()
    cursor.execute("SELECT `key` FROM languages WHERE active = 1")
    active_languages = cursor.fetchall()
    for lang in active_languages:
        cursor.execute("SELECT COUNT(*) FROM blessings WHERE language = %s", (lang[0],))
        (count,) = cursor.fetchone()
        assert count >= 366, f"Active language {lang[0]} should have at least 366 blessings."
    cursor.close()

def test_full_calendar_for_years(db):
    cursor = db.cursor()
    years_to_test = [datetime.now().year, datetime.now().year + 1]
    for year in years_to_test:
        start_date = datetime(year, 1, 1)
        end_date = datetime(year, 12, 31)
        cursor.execute("SELECT COUNT(DISTINCT date) FROM calendar WHERE date BETWEEN %s AND %s", (start_date, end_date))
        (days_count,) = cursor.fetchone()
        assert days_count == (end_date - start_date).days + 1, f"Year {year} should have a full calendar."
    cursor.close()

def test_language_reference_integrity(db):
    cursor = db.cursor()
    cursor.execute("""
    SELECT COUNT(*) FROM blessings
    WHERE language NOT IN (SELECT `key` FROM languages)
    """)
    (invalid_refs,) = cursor.fetchone()
    assert invalid_refs == 0, "All blessings must reference a valid language key."
    cursor.close()

def test_calendar_integrity(db):
    cursor = db.cursor()
    cursor.execute("""
    SELECT COUNT(*) FROM calendar
    WHERE blessing NOT IN (SELECT id FROM blessings)
    OR language NOT IN (SELECT `key` FROM languages)
    """)
    (invalid_calendar_entries,) = cursor.fetchone()
    assert invalid_calendar_entries == 0, "All calendar entries must reference valid blessings and languages."
    cursor.close()

def test_no_duplicate_blessings_per_day_per_language(db):
    """Ensure there are no duplicate blessings for any given language on the same day."""
    cursor = db.cursor()
    cursor.execute("""
    SELECT date, language, COUNT(DISTINCT blessing) as unique_blessings, COUNT(blessing) as total_blessings
    FROM calendar
    GROUP BY date, language
    HAVING unique_blessings != total_blessings
    """)
    result = cursor.fetchall()
    assert len(result) == 0, "There should be no duplicate blessings for the same language on any day."
    cursor.close()

def test_at_least_one_blessing_per_day_per_language(db):
    """Test that there is at least one blessing per day per active language in the calendar."""
    cursor = db.cursor()
    cursor.execute("""
    SELECT l.key, c.date
    FROM languages l
    CROSS JOIN (SELECT DISTINCT date FROM calendar WHERE date BETWEEN CURDATE() AND CURDATE() + INTERVAL 1 YEAR) d
    LEFT JOIN calendar c ON l.key = c.language AND d.date = c.date
    WHERE l.active = True AND c.date IS NULL
    """)
    missing_entries = cursor.fetchall()
    assert len(missing_entries) == 0, "Every active language should have at least one blessing each day for the next year."
    cursor.close()

def test_blessing_language_consistency(db):
    """Test that blessings are consistent with the language specified in calendar entries."""
    cursor = db.cursor()
    cursor.execute("""
    SELECT c.date, c.language, b.language
    FROM calendar c
    JOIN blessings b ON c.blessing = b.id
    WHERE b.language != c.language
    """)
    inconsistent_entries = cursor.fetchall()
    assert len(inconsistent_entries) == 0, "The language of blessings in the calendar must match the calendar entry language."
    cursor.close()

def test_update_permissions(db):
    """ Test that UPDATE queries are blocked. """
    cursor = db.cursor()
    try:
        cursor.execute("UPDATE languages SET name = 'Test Update' WHERE `key` = 'en'")
    except mysql.connector.Error as err:
        assert "update command denied" in str(err).lower(), "UPDATE operation should be denied."
    finally:
        cursor.close()

def test_insert_permissions(db):
    """ Test that INSERT queries are blocked. """
    cursor = db.cursor()
    try:
        cursor.execute("INSERT INTO languages (`key`, name, active) VALUES ('ZZ', 'Test Insert', True)")
    except mysql.connector.Error as err:
        assert "insert command denied" in str(err).lower(), "INSERT operation should be denied."
    finally:
        cursor.close()

def test_delete_permissions(db):
    """ Test that DELETE queries are blocked. """
    cursor = db.cursor()
    try:
        cursor.execute("DELETE FROM languages WHERE `key` = 'ZZ'")
    except mysql.connector.Error as err:
        assert "delete command denied" in str(err).lower(), "DELETE operation should be denied."
    finally:
        cursor.close()